#include<stdlib.h>
#include<stdio.h>

#define NPoints 1000000000
#define RADIO 1

int punto_in_circle(){
  float x = drand48();
  float y = drand48();
  return (x*x + y*y) <= RADIO;
}

double piCalculation(void){
  int cont=0;
  for (size_t i = 0; i < NPoints; i++){
    cont+=punto_in_circle();
  }

  return (4*(float)cont)/NPoints;
}

int main(void){
  double pi;
  // Seed setting
  srandom(4);

  pi = piCalculation();

  printf("Approximación de pi con %d puntos es: %'.10f",NPoints,pi);

  return 0;
}
